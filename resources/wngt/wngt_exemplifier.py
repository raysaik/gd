import xml.etree.ElementTree as ET
import copy

from xml.etree.ElementTree import Element, SubElement, Comment
from bs4 import BeautifulSoup
import re

def splitWNGT():
    tree = ET.parse("resources/wngt/wngt.xml")
    newRoot = ET.Element("root")
    root = tree.getroot()
    newTree = ET.ElementTree(newRoot)
    i=0
    for ins in root.findall(".//sentence"):
        wn30Original=ins.attrib["wn30_key"]
        if ";" in wn30Original:
            senses= wn30Original.split(";")
            for sense in senses:
                dupe=copy.deepcopy(ins)
                dupe.set("wn30_key",sense)
                newRoot.append(dupe)
        print (i)
        i+=1
        if i==15:
            break
    newTree.write("resources/wngt/wngt_2.xml")


splitWNGT()

