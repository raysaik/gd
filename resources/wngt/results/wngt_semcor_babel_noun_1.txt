Time taken to perform the test: 0:34:55.398731
                                    precision    recall  f1-score   support

                           Animals       0.71      0.64      0.68       143
Art, architecture, and archaeology       0.65      0.45      0.53       128
                           Biology       0.84      0.68      0.75       461
  Business, economics, and finance       0.74      0.62      0.68        96
          Chemistry and mineralogy       0.76      0.50      0.60       159
                         Computing       0.61      0.44      0.51        43
               Culture and society       1.00      1.00      1.00         1
                         Education       0.49      0.38      0.42        56
        Engineering and technology       0.71      0.69      0.70        32
                           Farming       0.91      0.64      0.75        47
                    Food and drink       0.62      0.60      0.61        57
             Games and video games       0.29      0.31      0.30        13
              Geography and places       0.80      0.82      0.81        93
            Geology and geophysics       0.68      0.87      0.76        15
               Health and medicine       0.67      0.73      0.70       157
 Heraldry, honors, and vexillology       0.67      0.62      0.64        13
                           History       1.00      0.33      0.50         3
          Language and linguistics       0.35      0.66      0.46       136
                     Law and crime       0.59      0.55      0.57       134
            Literature and theatre       0.69      0.72      0.70        53
                       Mathematics       0.50      0.58      0.54       280
                             Media       0.68      0.45      0.55        33
                       Meteorology       0.86      0.60      0.71        10
                             Music       0.51      0.46      0.49        78
        Numismatics and currencies       0.75      0.67      0.71         9
         Philosophy and psychology       0.68      0.74      0.71       504
             Physics and astronomy       0.70      0.79      0.74       298
           Politics and government       0.69      0.81      0.74        43
 Religion, mysticism and mythology       0.64      0.72      0.68       122
              Royalty and nobility       0.56      0.71      0.63         7
              Sport and recreation       0.56      0.49      0.52        57
              Textile and clothing       0.62      0.68      0.65        84
              Transport and travel       1.00      0.65      0.79        37
               Warfare and defense       0.70      0.68      0.69        98

                         micro avg       0.65      0.65      0.65      3500
                         macro avg       0.68      0.63      0.64      3500
                      weighted avg       0.67      0.65      0.66      3500

 Confusion Matrix 
[[ 92,   0,   3, ...,   5,   0,  14],
 [  4,  58,   0, ...,   2,   0,   4],
 [  0,   3, 314, ...,   2,   0,   0],
 ...,
 [  0,   2,   7, ...,  57,   0,   0],
 [  0,   0,   0, ...,   0,  24,   0],
 [  0,   0,   0, ...,   0,   0,  67]]