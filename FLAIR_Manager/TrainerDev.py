import torch
import apex

from pytorch_pretrained_bert.tokenization import BertTokenizer

from fast_bert.data import BertDataBunch
from fast_bert.learner import BertLearner
from fast_bert.metrics import accuracy

Path='Data'
DATA_PATH = 'Data/Data_SemCor'    # path for data files (train and val)
LABEL_PATH = 'Data/Data_WordNet/babeldomains_wordnet.txt'  # path for labels file
MODEL_PATH='Data/Data_SemCor'   # path for model artifacts to be stored
LOG_PATH='Data/Data_SemCor'     # path for log files to be stored

# location for the pretrained BERT models
BERT_PRETRAINED_PATH = Path('../../bert_models/pretrained-weights/uncased_L-12_H-768_A-12/')

args = {
    "max_seq_length": 512,
    "do_lower_case": True,
    "train_batch_size": 32,
    "learning_rate": 6e-5,
    "num_train_epochs": 12.0,
    "warmup_proportion": 0.002,
    "local_rank": -1,
    "gradient_accumulation_steps": 1,
    "fp16": True,
    "loss_scale": 128
}