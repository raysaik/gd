from flair.data import Sentence
from flair.models import TextClassifier


classifier = TextClassifier.load('resources/taggers/ag_news/final-model.pt')

# create example sentence
sentence = Sentence('In 1992 Perot tried to organize a third party at the national level.')
sentence2=Sentence('an organization to gain political power')
# predict class and print
classifier.predict(sentence)
classifier.predict(sentence2)

print(sentence.labels)
print(sentence2.labels)

sentence3=Sentence('the party of the first part')

sentence4=Sentence('a person involved in legal proceedings')
classifier.predict(sentence3)
classifier.predict(sentence4)

print(sentence3.labels)
print(sentence4.labels)


sentence5=Sentence('in the fall of 1973')

sentence6=Sentence('the season when the leaves fall from the trees')


classifier.predict(sentence5)
classifier.predict(sentence6)

print(sentence5.labels)
print(sentence6.labels)