import flair
import torch
from flair.data import Corpus
from flair.datasets import CSVClassificationCorpus
from flair.embeddings import DocumentRNNEmbeddings,BytePairEmbeddings, NILCEmbeddings, RoBERTaEmbeddings, BertEmbeddings, WordEmbeddings, \
    FlairEmbeddings, FastTextEmbeddings, ELMoEmbeddings
from flair.models import TextClassifier
from flair.trainers import ModelTrainer

# this is the folder in which train, test and dev files reside
data = '/home/aortuondo/pycharm/GlossDisambiguation/Data/Data_SemCor'
# column format indicating which columns hold the text and label(s)
column_name_map = {2: "text", 8: "label_topic"}
#entregar flair, resultados y como se obtienen
#Load cuda
device = None
if torch.cuda.is_available():
    device = torch.device('cuda:0')
else:
    device = torch.device('cpu')
flair.device = torch.device('cuda:0')

# load corpus containing training, test and dev data and if CSV has a header, you can skip it

corpus: Corpus = CSVClassificationCorpus(data,
                                         column_name_map,
                                         skip_header=True,
                                                                                 )

embeddings_storage_mode='gpu'
print(corpus.obtain_statistics())
# 2. create the label dictionary
label_dict = corpus.make_label_dictionary()
roberta_embeddings =RoBERTaEmbeddings(pretrained_model_name_or_path="roberta-base", layers="0,1,2,3,4,5,6,7,8,9,10",
                              pooling_operation="first_last", use_scalar_mix=True)
#w2v=NILCEmbeddings(embeddings='word2vec',model='cbow',size=300)
#bert_embedding = BertEmbeddings(bert_model_or_path='bert-base-multilingual-cased',layers="0,1,2,3,4,5")
#distilbert = BertEmbeddings(bert_model_or_path="distilbert-base-uncased",layers="0,1,2,3,4,5")
#ftt_embedding = FastTextEmbeddings('resources/cc.en.300.bin')
elmo=ELMoEmbeddings('small')
# 3. make a list of word embeddings
word_embeddings = [#WordEmbeddings('glove'),
                   #ftt_embedding
                  ## # comment in flair embeddings for state-of-the-art results
                   # FlairEmbeddings('news-forward'),
                   # FlairEmbeddings('news-backward'),
                   #roberta_embeddings
                   #bert_embedding
                   # distilbert
                    elmo
                    ### standard FastText word embeddings for English
                    #WordEmbeddings('en'),
                    ### Byte pair embeddings for English
                    #BytePairEmbeddings('en'),
                   ]

# 4. initialize document embedding by passing list of word embeddings
# Can choose between many RNN types (GRU by default, to change use rnn_type parameter)
document_embeddings: DocumentRNNEmbeddings = DocumentRNNEmbeddings(word_embeddings,
                                                                     hidden_size=512,
                                                                     reproject_words=True,
                                                                     reproject_words_dimension=256,

                                                                     )

# 5. create the text classifier
classifier = TextClassifier(document_embeddings, label_dictionary=label_dict,multi_label=False)

# 6. initialize the text classifier trainer
trainer = ModelTrainer(classifier, corpus)

# 7. start the training
trainer.train('resources/taggers/ag_news',
              learning_rate=0.1,
              mini_batch_size=32,
              anneal_factor=0.5,
              patience=5,
              max_epochs=10)

# 8. plot weight traces (optional)
from flair.visual.training_curves import Plotter
plotter = Plotter()
plotter.plot_weights('resources/taggers/ag_news/weights.txt')
#TODO
#entregar archivos por Drive