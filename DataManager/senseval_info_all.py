import xml.etree.ElementTree as ET
from tqdm import tqdm

def resultList():
    senses = open("Data/Data_SemCor/semcor.gold.key.txt", "r")
    resultsSense = {}
    for line in senses:
        lerroa=line.split(" ")
        senseList = []
        for el in lerroa:
            if lerroa[0]==el:
                w_id=el.rstrip()
            else:
                senseList.append(el.rstrip())
            resultsSense[w_id]=senseList
    senses.close()
    return resultsSense

def put_info_into_data():
    tree = ET.parse("Data/Data_SemCor/devSemcor.xml")
    root = tree.getroot()
    senseList=resultList()
    for ins in root.iter("instance"):
        _temp_dict = ins.attrib
        if "id" in _temp_dict:
            id = _temp_dict['id']
            if "NOUN" in _temp_dict['pos']:
                senses=senseList.get(id)
                ins.attrib['wn30_key'] = senses[0]
    tree.write('resources/semcor/dev_Semcor.xml')


put_info_into_data()