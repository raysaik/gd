##AQUI SE REUNEN LOS DATOS, CAMBIADN LA VARIABLE DE ENTRADA Y SALIDA

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment
from bs4 import BeautifulSoup
import re


#creates a list with all the nouns in the mcr
def get_allGloses():
    f = open("Data/Data_WordNet/wei_eng-30_examples.tsv")
    lista={}
    for line in f:
        lerroa=line.split("\t")
        word=lerroa[0]
        gloss=lerroa[2]
        tag=lerroa[3]
        wnid=lerroa[4].split("-")[2]
        if 'n' in tag:
            lista[wnid]= gloss,word,wnid
    f.close()
    return lista


def writeXML():
    examples=get_allGloses()
    root = ET.Element('root')
    tree = ET.ElementTree(root)
    for elem in examples:
        sen = SubElement(root, "sentence")
        example=examples.get(elem)
        frase=example[0]
        palabra=example[1]
        wn=example[2]
        sen.text=frase
        sen.set("wn",wn)
        sen.set("wordToBeDetermined",palabra)
        sep = re.findall(r"[\w']+|[.,!?;]", frase)
        sep.append(".")
        if "_" in palabra:
            palabras=palabra.split("_")
            lp=len(palabras)
            for pal in palabras:
                for n,item in enumerate(sep):
                    if pal in item:
                        sep[n]=palabra+"__"+wn
                        for x in range(1,lp):
                            print(sep)
                            sep.pop(n + 1)
                        break
                    break
                break
        else:
            for n,item in enumerate(sep):
                if palabra in item:
                    sep[n]=item+"__"+wn

        for w in sep:
            if len(w.split("__")) != 1:
                insta = w.split("__")
                instance = ET.SubElement(sen, "instance")
                instance.set("word", insta[0])
                instance.set("wn30_key", insta[1])
            else:
                wf = ET.SubElement(sen, "wf")
                wf.set("word", w)
    tree.write("Data/Data_Gloss/dataRaw.xml")

def prettify():

    x = open("Data/Data_Gloss/dataRaw.xml")
    with open("Data/Data_Gloss/data.xml", "w") as file:
        file.write(BeautifulSoup(x, "xml").prettify())
    file.close()
    x.close()


writeXML()
prettify()


