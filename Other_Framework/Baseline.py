import xml.etree.ElementTree as ET

import sklearn.metrics
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
import torch
import time
from joblib import dump, load
import pickle
import glob
import argparse
import numpy as np
import re
from pytorch_pretrained_bert import BertTokenizer, BertModel
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from nltk.stem import WordNetLemmatizer

from tqdm import tqdm, trange
from copy import deepcopy
import warnings

warnings.filterwarnings('ignore')


class BERT:

    def __init__(self, device_number='cuda:2', use_cuda=True):
        self.device_number = device_number
        self.use_cuda = use_cuda

        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

        self.model = BertModel.from_pretrained('bert-base-uncased')
        self.model.eval()

        if use_cuda:
            self.model.to(device_number)


class Word_Sense_Model:

    def __init__(self, device_number='cuda:2', use_cuda=True):

        self.device_number = device_number
        self.use_cuda = use_cuda
        self.sense_number_map = {'N': 1, 'V': 2, 'J': 3, 'R': 4}

        self.Bert_Model = BERT(device_number, use_cuda)
        self.lemmatizer = WordNetLemmatizer()

    def open_xml_file(self, file_name):

        tree = ET.parse(file_name)
        root = tree.getroot()

        return root, tree

    def readIndex(self):
        indexSense = {}
        index = open("./Data/Data_WordNet/index.sense", "r")
        for line in index:
            if not re.search("^ {2}", line):
                (name, wordnet, n1, n2) = line.split(" ")
                indexSense[name] = wordnet
        index.close()
        return indexSense

    def readDomains(self):
        f = open("Data/Data_WordNet/wei_ili_to_domains.tsv")
        domains = {}
        for line in f:
            column = line.split("\t")
            domain = column[0]
            ili = column[1].split("-")
            wordnet = ili[2]
            tag = ili[3]
            if 'n' in tag:
                domains[wordnet] = domain
        f.close()
        return domains

    def getBabelDomainList(self):
        bbdom = open("Data/Data_WordNet/babeldomains_wordnet.txt", "r")
        list = {}
        with bbdom as f:
            for line in f:
                lerroa = line.split("\t")
                (key, val) = lerroa[0], lerroa[1]
                if "n" in key:
                    list[key.replace('n', '')] = val
        return list

    def semeval_sent_sense_collect(self, xml_struct):

        _sent = []
        _sent1 = ""
        _senses = []
        pos = []

        for idx, j in enumerate(xml_struct.iter('word')):

            _temp_dict = j.attrib

            if 'lemma' in _temp_dict:

                words = _temp_dict['lemma'].lower()

            else:

                words = _temp_dict['surface_form'].lower()

            if '*' not in words:

                _sent1 += words + " "

                _sent.extend([words])

                if 'pos' in _temp_dict:
                    pos.extend([_temp_dict['pos']] * len([words]))

                else:
                    pos.extend([0] * len([words]))

                if 'wn30_key' in _temp_dict:

                    _senses.extend([_temp_dict['wn30_key']] * len([words]))

                else:
                    _senses.extend([0] * len([words]))

        return _sent, _sent1, _senses, pos

    def semcor_sent_sense_collect(self, xml_struct, ):

        _sent = []
        _sent1 = ""
        _senses = []
        temp_list_pos = []

        for idx, j in enumerate(xml_struct.iter('word')):

            _temp_dict = j.attrib
            flag = 0

            if 'lemma' not in _temp_dict:

                words = _temp_dict['surface_form'].lower()

                _sent1 += words + " "

                words = words.split('_')

                words1 = words[0:1]
                words2 = words[1:]

            else:

                _pos = _temp_dict['pos'].lower()[0]

                if _pos not in ['a', 'v', 'n']:
                    _pos = 'n'

                w2 = _temp_dict['lemma'].lower().split('_')
                words = _temp_dict['surface_form'].lower()

                _sent1 += words + " "

                words = words.split('_')

                l = self.lemmatizer.lemmatize(words[0], pos=_pos)
                if str(l).startswith(w2[0]) or str(w2[0]).startswith(l):

                    words1 = words[0:1]
                    words2 = words[1:]
                else:
                    flag = 1

            _sent.extend(words)

            if 'wn30_key' in _temp_dict:
                if not flag:

                    _senses.extend([_temp_dict['wn30_key']] * len(words1))
                    _senses.extend([0] * len(words2))
                else:
                    _senses.extend([0] * len(words))

            else:
                _senses.extend([0] * len(words))

        return _sent, _sent1, _senses, temp_list_pos

    def apply_bert_tokenizer(self, word):

        return self.Bert_Model.tokenizer.tokenize(word)

    def collect_bert_tokens(self, _sent, lemma=False):

        _bert_tokens = ['[CLS]', ]

        if lemma:

            for idx, j in enumerate(_sent):
                _sent[idx] = self.lemmatizer.lemmatize(_sent[idx])
                _tokens = self.apply_bert_tokenizer(_sent[idx])
                _bert_tokens.extend(_tokens)

        else:

            for idx, j in enumerate(_sent):
                _tokens = self.apply_bert_tokenizer(_sent[idx])

                _bert_tokens.extend(_tokens)

        _bert_tokens.append('[SEP]')

        return _bert_tokens

    def get_bert_embeddings(self, tokens):

        _ib = self.Bert_Model.tokenizer.convert_tokens_to_ids(tokens)
        _st = [0] * len(_ib)

        if self.use_cuda:

            _t1, _t2 = torch.tensor([_ib]).to(self.device_number), torch.tensor([_st]).to(self.device_number)

        else:
            _t1, _t2 = torch.tensor([_ib]), torch.tensor([_st])

        with torch.no_grad():

            _encoded_layers, _ = self.Bert_Model.model(_t1, _t2, output_all_encoded_layers=True)

            _e1 = _encoded_layers[-4:]

            _e2 = torch.cat((_e1[0], _e1[1], _e1[2], _e1[3]), 2)

            if self.use_cuda:
                _final_layer = _e2[0].cpu().numpy()

            else:
                _final_layer = _e2[0].cpu().numpy()

        return _final_layer

    def create_word_sense_maps(self, _word_sense_emb):

        _sense_emb = {}
        _sentence_maps = {}
        _sense_word_map = {}
        _word_sense_map = {}

        for i in _word_sense_emb:

            if i not in _word_sense_map:
                _word_sense_map[i] = []

            for j in _word_sense_emb[i]:

                if j not in _sense_word_map:
                    _sense_word_map[j] = []

                _sense_word_map[j].append(i)
                _word_sense_map[i].append(j)

                if j not in _sense_emb:
                    _sense_emb[j] = []
                    _sentence_maps[j] = []

                _sense_emb[j].extend(_word_sense_emb[i][j]['embs'])
                _sentence_maps[j].extend(_word_sense_emb[i][j]['sentences'])

        return _sense_emb, _sentence_maps, _sense_word_map, _word_sense_map

    def load_sklearn_embeddings(self, pickle_file_name, train_file, training_data_type,k):

        try:
            with open(pickle_file_name, 'rb') as h:
                _x = pickle.load(h)
            print("EMBEDDINGS FOUND!")
            return _x

        except:
            print("Embedding File Not Found!!")
            word_sense_emb = self.skLearnTrain(train_file, training_data_type,k)
            with open(pickle_file_name, 'wb') as h:
                pickle.dump(word_sense_emb, h)
            print("Embeddings Saved to " + pickle_file_name)
            return word_sense_emb

    def skLearnTrain(self,train_file,training_data_type,k):
        print("Training!")
        _word_sense_emb = {}
        _train_root, _train_tree= self.open_xml_file(train_file)
        #Training

        X_fitting,y_fitting=[],[]
        for i in tqdm(_train_root.iter('sentence')):
            if training_data_type == "SE":
                all_sent, all_sent1, all_senses, _ = self.semeval_sent_sense_collect(i)
                all_sent, all_sent1, all_senses = [all_sent], [all_sent1], [all_senses]

            elif training_data_type == "SEM":
                all_sent, all_sent1, all_senses, _ = self.semcor_sent_sense_collect(i)
                all_sent, all_sent1, all_senses = [all_sent], [all_sent1], [all_senses]

            elif training_data_type == "WNGT":
                all_sent, all_sent1, all_senses, _ = self.wngt_sent_sense_collect(i)

            else:
                print("Argument train_type not specified properly!!")
                quit()

            for sent, sent1, senses in zip(all_sent, all_sent1, all_senses):

                try:
                    bert_tokens = self.collect_bert_tokens(sent)
                    final_layer = self.get_bert_embeddings(bert_tokens)
                    count = 1
                    for idx, j in enumerate(zip(senses, sent)):
                        sense = j[0]
                        word = j[1]
                        if sense != 0:
                            embedding = np.mean(final_layer[count: count + len(self.apply_bert_tokenizer(word))], 0)
                            X_fitting.append(embedding)
                            y_fitting.append(sense)
                except Exception as e:
                    print(e)
        knnClassifier = KNeighborsClassifier(4,weights="distance")


        knnClassifier.fit(X_fitting,y_fitting)
        return knnClassifier

    def skLearnTest(self,train_file,test_file,emb_pickle_file,training_data_type,k):

        _word_sense_emb = {}
        _test_root, _test_tree = self.open_xml_file(test_file)

        knnClassifier = self.load_sklearn_embeddings(emb_pickle_file, train_file, training_data_type,k)
        print("Testing!")
        y_true,y_pred=[],[]
        for i in tqdm(_test_root.iter('sentence')):
            if training_data_type == "SE":
                all_sent, all_sent1, all_senses, _ = self.semeval_sent_sense_collect(i)
                all_sent, all_sent1, all_senses = [all_sent], [all_sent1], [all_senses]

            elif training_data_type == "SEM":
                all_sent, all_sent1, all_senses, _ = self.semcor_sent_sense_collect(i)
                all_sent, all_sent1, all_senses = [all_sent], [all_sent1], [all_senses]

            elif training_data_type == "WNGT":
                all_sent, all_sent1, all_senses, _ = self.wngt_sent_sense_collect(i)

            else:
                print("Argument train_type not specified properly!!")
                quit()

            for sent, sent1, senses in zip(all_sent, all_sent1, all_senses):

                try:
                    bert_tokens = self.collect_bert_tokens(sent)
                    final_layer = self.get_bert_embeddings(bert_tokens)
                    count = 1
                    for idx, j in enumerate(zip(senses, sent)):
                        sense = j[0]
                        word = j[1]
                        if sense != 0:
                            embedding = np.mean(final_layer[count: count + len(self.apply_bert_tokenizer(word))], 0)
                            y_true.append(sense)
                            y_pred.append(knnClassifier.predict(embedding.reshape(1,-1)))
                except Exception as e:
                    print(e)
        print(classification_report(y_true,y_pred))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WSD using BERT')
    parser.add_argument('--use_cuda', type=bool, default=True, help='Use GPU?')
    parser.add_argument('--device', type=str, default='cuda:2', help='GPU Device to Use?')
    parser.add_argument('--train_corpus', type=str, required=True, help='Training Corpus')
    parser.add_argument('--train_type', type=str, required=True, help='SEM/WNGT/SE')
    parser.add_argument('--trained_pickle', type=str,
                        help='Pickle file of Trained Bert Embeddings/Save Embeddings to this file')
    parser.add_argument('--test_corpus', type=str, required=True, help='Testing Corpus')
    parser.add_argument('--use_euclidean', type=int, default=0, help='Use Euclidean Distance to Find NNs?')
    parser.add_argument('--reduced_search', type=int, default=0, help='Apply Reduced POS Search?')

    args = parser.parse_args()

    print("Training Corpus is: " + args.train_corpus)
    print("Testing Corpus is:  " + args.test_corpus)
    print("Nearest Neighbour is: 4")

    print("Loading WSD Model!")

    WSD = Word_Sense_Model(device_number=args.device, use_cuda=args.use_cuda)

    print("Loaded WSD Model!")
    WSD.skLearnTest(train_file=args.train_corpus,
             test_file=args.test_corpus,
             training_data_type=args.train_type,
             emb_pickle_file=args.trained_pickle,
             k=4)
