import argparse
import xml.etree.ElementTree as ET
import re
class XML_Editor:

    def lexNumber2lexName(self,number):
        f = open("Data/lexNamesInfo.txt")

    def getAdimenSumo(self):
        #"ili-30-00001740-a capability"
        f=open("Data/adimenSUMO.txt")
        domains = {}
        for line in f:
            column = line.split(" ")
            domain = column[1].rstrip()
            ili = column[0].split("-")
            wordnet = ili[2]
            tag = ili[3]
            if 'n' in tag:
                domains[wordnet] = domain
        f.close()
        return domains

    def getLexNames(self):
        f=open("Data/sf.total")
        domains={}
        for line in f:
            column=line.split(" ")
            wn=column[0].split("-")[0]
            tag=column[0].split("-")[1]
            lex=column[1]
            if 'n' in tag:
                domains[wn] = lex[:-1]
        f.close()
        return domains

    def readIndex(self):
        indexSense = {}
        index = open("./Data/Data_WordNet/index.sense", "r")
        for line in index:
            if not re.search("^ {2}", line):
                (name, wordnet, n1, n2) = line.split(" ")
                indexSense[name] = wordnet
        index.close()
        return indexSense

    def getWordnetDomains(self):
        f = open("Data/Data_WordNet/wei_ili_to_domains.tsv")
        domains = {}
        for line in f:
            column = line.split("\t")
            domain = column[0].rstrip()
            ili = column[1].split("-")
            wordnet = ili[2]
            tag = ili[3]
            if 'n' in tag:
                domains[wordnet] = domain
        f.close()
        return domains

    def getBabelDomainList(self):
        bbdom = open("Data/Data_WordNet/babeldomains_wordnet.txt", "r")
        list = {}
        with bbdom as f:
            for line in f:
                lerroa = line.split("\t")
                (key, val) = lerroa[0], lerroa[1]
                if "n" in key:
                    list[key.replace('n', '')] = val
        return list

    def changeValuesSemcor(self,xmlFile,outputFile,domainType):
        index=self.readIndex()
        if(domainType=="Babel"):
            domain=self.getBabelDomainList()
        elif(domainType=="Wordnet"):
            domain=self.getWordnetDomains()
        elif(domainType=="Lexicography"):
            domain=self.getLexNames()
        elif (domainType == "Sumo"):
            domain = self.getAdimenSumo()
        else:
            exit(-1)
        _tree = ET.parse(xmlFile)
        _root = _tree.getroot()

        for idx, j in enumerate(_root.iter('instance')):
            _temp_dict = j.attrib
            if 'wn30_key' in _temp_dict:
                sense=_temp_dict['wn30_key']
                wDomain=domain.get(index.get(sense))
                if wDomain is not None:
                    _temp_dict['wn30_key']=wDomain
                else:
                    del _temp_dict["wn30_key"]

        _tree.write(outputFile)

    def changeValuesSemEval(self, xmlFile, outputFile,domainType):
        index = self.readIndex()
        if (domainType == "Babel"):
            domain = self.getBabelDomainList()
        elif (domainType == "Wordnet"):
            domain = self.getWordnetDomains()
        elif (domainType == "Lexicography"):
            domain = self.getLexNames()
        elif (domainType == "Sumo"):
            domain = self.getAdimenSumo()
        else:
            exit(-1)
        _tree = ET.parse(xmlFile)
        _root = _tree.getroot()
        for idx, j in enumerate(_root.iter('word')):
            _temp_dict = j.attrib
            if 'wn30_key' in _temp_dict:
                sense = _temp_dict['wn30_key']
                wDomain = domain.get(index.get(sense))
                if wDomain is not None:
                    _temp_dict['wn30_key'] = wDomain
                else:
                    del _temp_dict['wn30_key']
        _tree.write(outputFile)

    def changeValuesSemEvalALL(self, xmlFile, outputFile, domainType):
        index = self.readIndex()
        if (domainType == "Babel"):
            domain = self.getBabelDomainList()
        elif (domainType == "Wordnet"):
            domain = self.getWordnetDomains()
        elif (domainType == "Lexicography"):
            domain = self.getLexNames()
        elif (domainType == "Sumo"):
            domain = self.getAdimenSumo()
        else:
            exit(-1)
        _tree = ET.parse(xmlFile)
        _root = _tree.getroot()
        for idx, j in enumerate(_root.iter('instance')):
            _temp_dict = j.attrib
            if 'wn30_key' in _temp_dict:
                sense = _temp_dict['wn30_key']
                wDomain = domain.get(index.get(sense))
                if wDomain is not None:
                    _temp_dict['wn30_key'] = wDomain
                else:
                    del _temp_dict['wn30_key']
        _tree.write(outputFile)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WSD using BERT')
    parser.add_argument('--xmlFile', type=str, required=True, help='XML File to be changed')
    parser.add_argument('--type', type=str, required=True, help='SEM for SEMcor, SE for SemEval')
    parser.add_argument('--outputXML', type=str, required=True, help='XML File to be saved into')
    #TODO rellenar esto
    parser.add_argument('--domainType',type=str,required=True,help="Domain type to be formatted into, it can be BabelDOmains, WordnetDomains,etc")
    args = parser.parse_args()

    editor=XML_Editor()
    if(args.type=='SEM'):
        editor.changeValuesSemcor(xmlFile=args.xmlFile,outputFile=args.outputXML,domainType=args.domainType)
    if(args.type=='SE'):
        editor.changeValuesSemEval(xmlFile=args.xmlFile,outputFile=args.outputXML,domainType=args.domainType)
    if (args.type == 'OMSTI'):
        editor.changeValuesSemEvalALL(xmlFile=args.xmlFile, outputFile=args.outputXML, domainType=args.domainType)