import random
import copy
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
from lxml import etree
from bs4 import BeautifulSoup


def split_Data():
    tree = ET.parse("data.xml")
    root = tree.getroot()
    train = ET.Element("root")
    test = ET.Element("root")
    dev = ET.Element("root")
    trtrain = ET.ElementTree(train)
    tetrain = ET.ElementTree(test)
    detrain = ET.ElementTree(dev)
    i=0
    for ins in root.findall("./sentence"):
        i+=1
        print(i)
        dupe=copy.deepcopy(ins)
        r = random.randint(1, 101)
        if r < 90:
            train.append(dupe)
        elif r <= 95:
            dev.append(dupe)
        else:
            test.append(dupe)
        ins.clear()
    trtrain.write("trainEKDRaw.xml")
    detrain.write("devEKDRaw.xml")
    tetrain.write("testEKDRaw.xml")

def prettify(file):
    x = open(file)
    with open(file.replace("Raw",""), "w") as file2:
        file2.write(BeautifulSoup(x, "xml").prettify())
    file2.close()
    x.close()

split_Data()
prettify("trainEKDRaw.xml")
prettify("devEKDRaw.xml")
prettify("testEKDRaw.xml")




