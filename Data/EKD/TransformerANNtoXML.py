import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment
from bs4 import BeautifulSoup
import re



def getSentences():
    sentenceFile = open("training/scenario.txt","r")
    sentenceList={}
    i=0
    for line in sentenceFile:
        sentenceList[i]=line.rstrip(), len(line)
        i+=1
    sentenceFile.close()
    return sentenceList

def resultList():
    senses = open("training/scenario.ann", "r")
    sentences=getSentences()
    resultsSense = {}
    sId=0
    tLength=sentences.get(sId)[1]
    for line in senses:
        if line.startswith("T"):
            tab = line.split("\t")
            end_pos = tab[1].split(" ")[-1]
            if int(end_pos) < tLength:
                klase = tab[1].split(" ")[0]
                tier = tab[0].rstrip()
                wf = tab[2].rstrip()
                resultsSense[tier] = klase, wf, sId
            else:
                sId=sId+1
                tLength = tLength+sentences.get(sId)[1]
                klase = tab[1].split(" ")[0]
                tier = tab[0].rstrip()
                wf = tab[2].rstrip()
                resultsSense[tier] = klase, wf, sId

    senses.close()
    return resultsSense


def put_info_into_data():
    root = ET.Element('root')
    tree = ET.ElementTree(root)
    senses=resultList()
    sentences=getSentences()
    minisenses={}
    i=0
    for sentence in sentences:
        sen = SubElement(root, "sentence")
        fraseInfo= sentences.get(sentence)
        frase=fraseInfo[0]
        sen.text=frase
        for sense in senses:
            sen2=senses.get(sense)
            ins=sen2[1]
            if ins in frase and sen2[2]==i:
                minisenses[sense]=sen2[0],sen2[1]
        print (frase)
        sep= re.findall(r"[\w']+|[.,!?;]", frase)

        for mini in minisenses:
            elem=minisenses.get(mini)
            fr=elem[1]
            case=elem[0]
            ws=len(fr.split(" "))
            for n,item in enumerate(sep):
                if fr.split(" ")[0] == item:
                    sep[n]=fr+"__"+case
                    for x in range(1,ws):
                        sep.pop(n+1)

        for w in sep:
            if len(w.split("__"))!=1:
                insta=w.split("__")
                instance=ET.SubElement(sen,"instance")
                instance.set("word",insta[0])
                instance.set("wn30_key",insta[1])
            else:
                wf= ET.SubElement(sen,"wf")
                wf.set("word",w)
        minisenses.clear()
        i+=1
    tree.write("dataRaw.xml")

def prettify():
    x = open("dataRaw.xml")
    print(BeautifulSoup(x, "xml").prettify())
    x.close()
    x = open("dataRaw.xml")
    with open("data.xml", "w") as file:
        file.write(BeautifulSoup(x, "xml").prettify())
    file.close()
    x.close()

def substitute_elements():
    tree = ET.parse("data.xml")
    root = tree.getroot()







  #  for ins in root.iter("instance"):
  #      _temp_dict = ins.attrib
  #      if "id" in _temp_dict:
  #          id = _temp_dict['id']
  #          if "NOUN" in _temp_dict['pos']:
  #              senses=senseList.get(id)
  #              ins.attrib['wn30_key'] = senses[0]
    #tree.write('resources/test_Semcor.xml')


put_info_into_data()
prettify()
substitute_elements()